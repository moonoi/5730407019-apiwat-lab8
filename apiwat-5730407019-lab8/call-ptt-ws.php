<?php
header("Content-type:text/xml");
$soap = "http://www.pttplc.com/webservice/pttinfo.asmx?op=CurrentOilPrice";
$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
  <soap:Body>
    <CurrentOilPrice xmlns="http://www.pttplc.com/ptt_webservice/">
      <Language>EN</Language>
    </CurrentOilPrice>
  </soap:Body>
</soap:Envelope>';

$headers = array(
    "POST /webservice/pttinfo.asmx HTTP/1.1",
    "Host: www.pttplc.com",
    "Content-type: text/xml; charset=utf-8",
    "Content-Length: " . strlen($xml_post_string),
    "SOAPAction: \"http://www.pttplc.com/ptt_webservice/CurrentOilPrice\""
);
$url = $soap;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);
$response1 = str_replace("<soap:Body>", "", $response);
$response2 = str_replace("</soap:Body>", "", $response1);
$parser = simplexml_load_string($response2);
echo($parser->asXML());

?>


