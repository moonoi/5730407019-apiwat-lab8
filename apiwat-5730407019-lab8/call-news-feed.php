<?php

header('Content-type: application/xml');
$url = ('http://www.bangkokbiznews.com/rss/feed/technology.xml');
$getData = file_get_contents($url);


$FileXML = new SimpleXMLElement($getData);
$showXML = new SimpleXMLElement('<news></news>');
$showXML->addChild('channel_title', $FileXML->channel->title->__toString());



foreach ($FileXML->channel->item as $inItem) {
    # code...
    $item = $showXML->addChild('item');
    $item->addChild('title', $inItem->title->__toString());
    $item->addChild('link', $inItem->link->__toString());
}
echo $showXML->saveXML();
?>